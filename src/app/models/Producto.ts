export class Producto {
    nombre:string;
    precio:number;
    cantidad:number;
    categoria:string;
    descripcion:string;
    imagen:string;

    _id:string;
    imagePath: string;
    fechavencimiento:string;
    file:File;

    constructor(nombre:string,precio:number,cantidad:number,categoria:string,descripcion:string,imagen:string,_id:string,imagePath: string,fecha?:string, file?:File){

        this.nombre=nombre;
        this.precio=precio;
        this.cantidad=cantidad;
        this.categoria=categoria;
        this.imagen=imagen;
        this.descripcion=descripcion;
        
        this._id=_id;
        this.imagen=imagen;
        this.imagePath=imagePath;
        this.fechavencimiento=fecha;
        this.file=file;
    }
    public getFecha(){
        return this.fechavencimiento;
    }
    public getPrecio(): number {
        return this.precio;
    }
    public getCantidad(): number {
        return this.cantidad;
    }
    public getImagePath(): string {
        return this.imagePath;
    }
    public getDescripcion(): string {
        return this.descripcion;
    }
    public getcategoria(): string {
        return this.categoria;
    }
    public getId(): string {
        return this._id;
    }
    public getimagen(): string {
        return this.imagen;
    }
    public getNombre(): string{
        return this.nombre;
    }
    public getFile(): File{
        return this.file;
    }
}