import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

import { CategoryTableComponent } from './elementsMenu/category-table/category-table.component';
import { LaboratoryTableComponent } from './elementsMenu/laboratory-table/laboratory-table.component';
import { RegistroProductoComponent } from './elementsMenu/registro-producto/registro-producto.component';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'index',
    pathMatch: 'full'
  },
  {
    path: 'index',
    loadChildren: () => import('./pages/index/index.module').then(m => m.IndexPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./pages/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'signin',
    loadChildren: () => import('./pages/signin/signin.module').then(m => m.SigninPageModule)
  },
  {
    path: 'modal',
    loadChildren: () => import('./pages/modal/modal.module').then(m => m.ModalPageModule)
  },
  {
    path: 'select-mode',
    loadChildren: () => import('./pages/select-mode/select-mode.module').then(m => m.SelectModePageModule)
  },
  { path: 'category', component: CategoryTableComponent },
  { path: 'laboratory', component: LaboratoryTableComponent },
  { path: 'registroProducto', component: RegistroProductoComponent },


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
