import { NgModule } from '@angular/core';

// import {AfterViewInit, Component, ViewChild} from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CatalogoComponent, DialogContent } from './catalogo/catalogo.component';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';
import {MatDialog, MatDialogRef, MatDialogModule} from '@angular/material/dialog';

import {MAT_DIALOG_DATA} from '@angular/material/dialog';
import {MatGridListModule} from '@angular/material/grid-list'; 


@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    CatalogoComponent,
    
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    MatGridListModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    
  ],
  exports: [
    HeaderComponent,
    FooterComponent,
    CatalogoComponent
  ],
  providers: [
    {
      provide: MatDialogRef,
      useValue: {}
    },
    
 ],
})
export class ComponentsModule { }
