import { ComponentsModule } from 'src/app/components/components.module';
import { Component, OnInit, Input, Inject, ViewEncapsulation } from '@angular/core';
import { MatDialog} from '@angular/material/dialog';
import { MAT_DIALOG_DATA, MatDialogRef,} from '@angular/material/dialog';
import { StorageService } from 'src/app/services/storage.service';
import { FirebaseService } from 'src/app/services/firebase.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-catalogo',
  templateUrl: './catalogo.component.html',
  styleUrls: ['./catalogo.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CatalogoComponent implements OnInit {

  public catalogo: Array<any> = new Array ();
  public categoria: Array<any> = new Array ();  
  public todosLosProds: Array<any> = new Array();
  public nombreCategorias: Array<any> = new Array();
  public hayProducto:boolean = true;  
  message:string;
  errorMessage:string;
  
  constructor(
    public dialog: MatDialog,
    public storage: StorageService,
    public firebaseService: FirebaseService,
    private data: DataService,
    ) {
      this.data.currentMessage.subscribe(message => this.message = message);
    }

  ngOnInit() {    
    this.firebaseService.getProductos()
    .subscribe(result => {      
      this.todosLosProds = result;
    });
    this.firebaseService.getCategorias()
    .subscribe(result => {      
      this.nombreCategorias = result;
    });
    this.data.currentMessage.subscribe(message => this.message = message);  
  }  

  mostrarCatalogo(){    
    this.catalogo.length = 0; 
    this.todosLosProds.push.apply(this.catalogo, this.todosLosProds);     
    console.log(this.catalogo.length);
    this.hayProductos(this.catalogo.length);  
  }

  mostrarCategoria(categoria){  
      this.errorMessage="NO HAY PRODUCTOS EN ESTA CATEGORIA";      
      this.categoria.length = 0;                     
      this.todosLosProds.forEach(producto => {                 
      if(categoria.toString().toUpperCase() === producto.payload.doc.data().categoria.toString().toUpperCase())   {
          this.categoria.push(producto);       
        }       
      });     
      this.catalogo.length = 0; 
      this.categoria.push.apply(this.catalogo, this.categoria);  
      console.log(this.catalogo.length);
      this.hayProductos(this.catalogo.length);  
  }

    
    
  mostrarNombre(nombre){   
    this.errorMessage="NO HAY PRODUCTOS CON ESE NOMBRE";        
    this.categoria.length = 0;                     
    this.todosLosProds.forEach(producto => {                 
      if(producto.payload.doc.data().nombre.toString().toUpperCase().includes(nombre.toString().toUpperCase().trim()))   {
        this.categoria.push(producto);       
      }       
    });     
    this.catalogo.length = 0; 
    this.categoria.push.apply(this.catalogo, this.categoria);  
    console.log(this.catalogo.length);
    this.hayProductos(this.catalogo.length);  
  }
  hayProductos(cantidad){
    this.hayProducto = cantidad > 0; 
  }

  openDialog(producto): void {    
    const dialogRef = this.dialog.open(DialogContent, {
      width: '700px',
      height: '400px',
      panelClass: 'my-dialog',
      data: producto,
    });   
  }    
}


@Component({
  selector: 'dialog-content-example-dialog',
  templateUrl: 'catalogo-content.html',
  styleUrls: ['./catalogo.component.scss'],
})

export class DialogContent {

 
  constructor(
    public dialogRef: MatDialogRef<DialogContent>,
    @Inject(MAT_DIALOG_DATA) public data: any){
  }
  getNombre(){
    return this.data.payload.doc.data().nombre;
  }

  getDescripcion(){
    return this.data.payload.doc.data().descripcion;
  }

  getPrecio(){
    return this.data.payload.doc.data().precio;
  }

  getFabricante(){
    return this.data.payload.doc.data().fabricante;;
  }

  getCategoria(){
    return this.data.payload.doc.data().categoria;
  }

  estaAgotado(){
    if(this.data.payload.doc.data().cantidad <= 0){
      return ('Agotado');
    }else{
      return ('Disponible');
    }
    
  }  
  
  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    console.log(this.data);    
  }
}
