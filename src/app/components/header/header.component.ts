import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { LoginPage } from 'src/app/pages/login/login.page';
import { ModalPage } from 'src/app/pages/modal/modal.page';
import { SigninPage } from 'src/app/pages/signin/signin.page';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';
import { DataService } from 'src/app/services/data.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {

  public isLoggedIn: boolean = false;
  public nombre: string;
  public apellido: string;
  public tipo: string;
  public message:string;
  constructor(
    public modalController: ModalController,
    public storage: StorageService,
    public afAuth: AuthService,
    public router: Router,
    public firestore: AngularFirestore,
    private data: DataService
  ) { }

  async ngOnInit() {
    const user = await this.afAuth.getCurrentUser();
    console.log('User: ', user.email);
    this.data.currentMessage.subscribe(message => this.message = message);
    this.firestore.collection('usuarios').doc(user.email).valueChanges().subscribe(res => {
      console.log(res);
      this.nombre = res['nombre'];
      this.apellido = res['apellido'];
      this.tipo = res['tipo'];
    });

    if (user) {
      this.isLoggedIn = true;
    } else {
      this.isLoggedIn = false;
    }
  }

  ngOnChange() {
    
  }

  newMessage() {
    this.data.changeMessage(this.message)
  }

  printSearchbar(evt){
    console.log(evt.srcElement.value);
    this.message = evt.srcElement.value
    this.newMessage();
    //return evt.srcElement.value;
  }

  async login() {
    const modal = await this.modalController.create({
      component: LoginPage,
      componentProps: {}
    });
    return await modal.present();
  }

  async signin() {
    const modal = await this.modalController.create({
      component: SigninPage,
      componentProps: {}
    });
    return await modal.present();
  }

  async modal() {
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: {}
    });
    return await modal.present();
  }

  logout() {
    this.afAuth.logout();
    this.storage.remove('user');
  }

  goToHome() {
    this.router.navigate(['home']);
  }
}
