import {AfterViewInit, Component, ViewChild, OnInit, ChangeDetectorRef} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AngularFirestore } from '@angular/fire/firestore';
import {ToastService } from 'src/app/services/toast.service';
import { FirebaseService } from 'src/app/services/firebase.service';

export interface LaboratoryData {
  id: string;
  laboratorio: string;
  telf : string;
}

@Component({
  selector: 'app-laboratory-table',
  templateUrl: './laboratory-table.component.html',
  styleUrls: ['./laboratory-table.component.scss'],
})
export class LaboratoryTableComponent implements AfterViewInit, OnInit {
  displayedColumns: string[] = ['id', 'laboratorio','telf'];
  laboratorios: LaboratoryData[]=[]; //tiene datos de los laboratorios
  dataSource = new MatTableDataSource<LaboratoryData>();
  public laboratoriosBD: Array<any> = new Array (); 
  public LaboratoryFormGroup: FormGroup;
  private numberPattern: any = /^[0-9]+$/;

  columnas = [
    { titulo:"Nro", name:"id"},
    { titulo:"Laboratorio / Proveedor",name: "laboratorio"},
    { titulo:"Telefono", name: "telf"},
  ]
 
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public formBuilder: FormBuilder,
    private firestore: AngularFirestore,
    public firebaseService: FirebaseService,
    public toast: ToastService,
    private changeDetectorRefs: ChangeDetectorRef
   ) {
    this.buildForm();
    
   }

    ngOnInit(){
      this.firebaseService.getLaboratorios().subscribe(result => { this.laboratoriosBD = result;
      }); 
      // this.actualizar();
    }


  buildForm(){
    this.LaboratoryFormGroup = this.formBuilder.group({
      laboratorio: ['', [Validators.required, Validators.maxLength(30)]],
      telf: ['', [Validators.required, Validators.maxLength(8), Validators.pattern(this.numberPattern) ]]
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  cargarTablaLaboratorios(){
    var idlab, nombrelab, telflab;
    var i=0;
    if(this.laboratorios.length==0){
      this.laboratoriosBD.forEach( lab => {
      idlab=lab.payload.doc.data().id;
      nombrelab=lab.payload.doc.data().laboratorio;
      telflab=lab.payload.doc.data().telf;
      this.laboratorios.push( { id: idlab , laboratorio: nombrelab , telf: telflab });
      let model = this.laboratorios[i];
      this.dataSource.data.push( model);
      this.dataSource._updateChangeSubscription();
      i++;
    });
   }
  }
   
  actualizar(){
    var removidos = this.laboratorios.splice(0, this.laboratorios.length );
    var removidos1 = this.dataSource.data.splice(0,this.dataSource.data.length);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.cargarTablaLaboratorios();
    // this.dataSource = new MatTableDataSource<LaboratoryData>();
    this.changeDetectorRefs.detectChanges();
  }

  alertRegistro(){
    this.toast.present("Registro de Laboratorio, Exitoso", 2000);
  }

  registrarLaboratorio(){
    var lab = this.LaboratoryFormGroup.get('laboratorio').value;
    var cel = this.LaboratoryFormGroup.get('telf').value;
    if( lab=="" || cel=="" || lab==null || cel==null){
      this.toast.present("Llene los campos obligatorios", 1000);
    }else{
      //actualizamos en la base
        this.firestore.collection('laboratorios').add({
        laboratorio:this.LaboratoryFormGroup.controls.laboratorio.value,
        telf:this.LaboratoryFormGroup.controls.telf.value,
        id: this.laboratorios.length + 1
        })
        
        // var bandera = this.laboratorios.length + 1;
        // this.laboratorios.push( { 
        //   id: bandera.toString(),
        //   laboratorio: this.LaboratoryFormGroup.controls.laboratorio.value,
        //   telf: this.LaboratoryFormGroup.controls.telf.value});

        // let model = this.laboratorios[bandera];
        // this.dataSource.data.push( model);
        // this.dataSource._updateChangeSubscription();
        // this.laboratorios=[];
        this.LaboratoryFormGroup.reset();
        this.alertRegistro();
        this.buildForm();
    }
    this.actualizar();
  }

  
}
