import {AfterViewInit, Component, ViewChild, OnInit} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import {FormBuilder, FormGroup, Validators } from '@angular/forms';
import {AngularFirestore } from '@angular/fire/firestore';
import {ToastService } from 'src/app/services/toast.service';
import {FirebaseService } from 'src/app/services/firebase.service';

export interface CategoryData {
  id: string;
  categoria: string;
  descripcion: string;
}
@Component({
  selector: 'app-category-table',
  templateUrl: './category-table.component.html',
  styleUrls: ['./category-table.component.scss'],
})


export class CategoryTableComponent implements AfterViewInit, OnInit{
  displayedColumns: string[] = ['id', 'categoria','descripcion'];
  dataSource = new MatTableDataSource<CategoryData>();
  public categorias: CategoryData[]=[]; Arr
  public categoriasBD: Array<any> = new Array (); 
  public CategoryFormGroup: FormGroup;
  columnas = [
    { titulo:"Nro", name:"id"},
    { titulo:"Categoria",name: "categoria"},
    { titulo:"Descripcion", name: "descripcion"},
  ]
  

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(
    public formBuilder: FormBuilder,
    public toast: ToastService,
    private firestore: AngularFirestore,
    public firebaseService: FirebaseService
   ) {
    this.buildForm();
    // this.cargarTablaCategorias();
   }

      ngOnInit() {    
        this.firebaseService.getCategorias().subscribe(result => { this.categoriasBD = result;
        });  
      }  



  buildForm(){
    this.CategoryFormGroup = this.formBuilder.group({
      categoria: ['', [Validators.required, Validators.maxLength(30)]],
      descripcion: ['', [Validators.required, Validators.maxLength(300)]]
    });
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  
  cargarTablaCategorias(){
    var idcat, nombrecat, desc;
    var i=0;
    if(this.categorias.length==0){
      this.categoriasBD.forEach( cat => {

      idcat=cat.payload.doc.data().id;
      nombrecat=cat.payload.doc.data().categoria;
      desc=cat.payload.doc.data().descripcion;
      this.categorias.push( { id: idcat , categoria: nombrecat , descripcion: desc });
      let model = this.categorias[i];
      this.dataSource.data.push( model);
      this.dataSource._updateChangeSubscription();
      i++;
    });
   }
  }

  alertRegistro(){
    this.toast.present("Registro de Categoria, Exitoso", 2000);
  }
  actualizar(){
    var removidos = this.categorias.splice(0, this.categorias.length );
    var removidos1 = this.dataSource.data.splice(0,this.dataSource.data.length);
    this.dataSource._updateChangeSubscription();
    this.dataSource._updatePaginator;
    this.cargarTablaCategorias();
  }

  registrarCategoria(){
    var cat = this.CategoryFormGroup.get('categoria').value;
    var desc = this.CategoryFormGroup.get('descripcion').value;
    if( cat=="" || desc=="" || cat==null || desc==null){
      this.toast.present("Llene los campos obligatorios", 1000);
    }else{
      this.firestore.collection('categorias').add({
      categoria:this.CategoryFormGroup.controls.categoria.value,
      descripcion:this.CategoryFormGroup.controls.descripcion.value,
      id: this.categorias.length + 1
      })
      this.CategoryFormGroup.reset();
      this.alertRegistro();
      this.buildForm();
    }
    
  }


}


