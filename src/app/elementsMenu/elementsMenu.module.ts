import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { NgxMatFileInputModule } from '@angular-material-components/file-input';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RegistroProductoComponent } from './registro-producto/registro-producto.component';
import { CategoryTableComponent } from './category-table/category-table.component';
import { LaboratoryTableComponent } from './laboratory-table/laboratory-table.component';
import { MatNativeDateModule } from '@angular/material/core';
// import { MatTableDataSource } from '@angular/material/table';
// import { ToastService } from 'src/app/services/toast.service';
// import { MatNativeDateModel }

@NgModule({
  declarations: [
    RegistroProductoComponent,
    CategoryTableComponent,
    LaboratoryTableComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatToolbarModule,
    NgxMatFileInputModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule
    // ToastService,


  ],
  exports: [

    RegistroProductoComponent,
    CategoryTableComponent,
    LaboratoryTableComponent
  ]
})

export class ElementsMenuModule { }
