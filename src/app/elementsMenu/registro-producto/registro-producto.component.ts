import { Component, ViewChild, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators, NgForm } from '@angular/forms';
import { ToastService } from 'src/app/services/toast.service';
import { AcceptValidator, MaxSizeValidator } from '@angular-material-components/file-input';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { StorageService } from 'src/app/services/storage.service';
import { finalize } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FirebaseService } from 'src/app/services/firebase.service';
import { MatSelectChange } from '@angular/material/select';
import { analyzeAndValidateNgModules } from '@angular/compiler';
import { UploadTaskSnapshot } from '@angular/fire/storage/interfaces';

interface HtmlInputEvent extends Event {
  target: HTMLInputElement & EventTarget;
}
export interface Opcion {
  value: string;
  viewValue: string;
}
@Component({
  selector: 'app-registro-producto',
  templateUrl: './registro-producto.component.html',
  styleUrls: ['./registro-producto.component.scss'],
})
export class RegistroProductoComponent implements OnInit {
  //@ViewChild('formDirective') private formDirective: NgForm;
  @ViewChild('myForm') myForm;
  registroProductoFormGroup: FormGroup;
  maxSize = 350;
  laboS = '';
  catS = '';

  downloadUrl$: Observable<string>;


  // uploadPercent: Observable<number>;
  // urlImage: Observable<string>;
  // public files;
  private image: any;
  fileControl: FormControl;

  private numberPattern: any = /^[0-9]+$/;
  dia = new Date().getDate();
  mes = new Date().getMonth();
  anio = new Date().getFullYear();
  min = new Date(this.anio, this.mes, this.dia);
  selected = 1;
  selectedCategory: string;
  selectedLaboratory: string;

  public categoriasBD: Array<any> = new Array();
  public laboratoriosBD: Array<any> = new Array();
  public laboratorios: Opcion[] = [];
  public categorias: Opcion[] = [];

  constructor(
    private formBuilder: FormBuilder,
    public toast: ToastService,
    public router: Router,
    private firestore: AngularFirestore,
    public storage: AngularFireStorage,
    public firebaseService: FirebaseService,
    // private formDirective: NgForm,
    //  public storage: StorageService,

  ) {
    this.buildForm();
  }


  ngOnInit() {
    this.firebaseService.getCategorias().subscribe(result => {
      this.categoriasBD = result;
    });
    this.firebaseService.getLaboratorios().subscribe(result => {
      this.laboratoriosBD = result;
    });

    this.fileControl.valueChanges.subscribe((files: any) => {
      if (!Array.isArray(files)) {
        this.image = [files];
        console.log(this.image, "if");

      } else {
        this.image = files;
        console.log(this.image, "else");

      }
    })
  }


  private buildForm() {
    this.registroProductoFormGroup = this.formBuilder.group({
      nombre: ['', [Validators.required, Validators.minLength(3), Validators.maxLength(30)]],
      cantidad: ['', [Validators.required, Validators.min(1), Validators.max(100), Validators.pattern(this.numberPattern)]],
      precio: ['', [Validators.required, Validators.min(1), Validators.max(9999), Validators.maxLength(4), Validators.pattern(this.numberPattern)]],
      descripcion: ['', [Validators.required, Validators.minLength(15), Validators.maxLength(500)]],
      imagen: ['', [Validators.required]],
      selectCat: ['', [Validators.required]],
      selectLab: ['', [Validators.required]],
      fechaV: ['', [Validators.required]]

    });

    this.fileControl = new FormControl(this.image, [
      Validators.required,
      MaxSizeValidator(this.maxSize * 1024)
    ])
    console.log(this.registroProductoFormGroup);
  }

  /* handleImage(event: any): void {
    console.log(event);

    this.image = event.target.files[0];
    console.log(this.image);

  } */
  //
  // onUpload(e){
  //   const id = Math.random().toString(36).substring(2);
  //   const file = e.target.files[0];
  //   const filePath = `upload/${id}`;
  //   const ref = this.storage.ref(filePath);
  //   const task = this.storage.upload(filePath,file);
  //   this.uploadPercent = task.percentageChanges();
  //   task.snapshotChanges().pipe(finalize (()=> this.urlImage = ref.getDownloadURL())).subscribe;
  // }
  checkFile() {
    let file = this.image[0];

    let path = 'images/' + this.registroProductoFormGroup.controls.nombre.value;
    let uploadTask: AngularFireUploadTask = this.storage.upload(path, file);

    uploadTask.then((uploadSnapshot: UploadTaskSnapshot) => {
      uploadSnapshot.ref.getDownloadURL().then((downloadURL) => {
        this.downloadUrl$ = downloadURL;
        this.registroProductoFormGroup.controls.imagen.setValue(this.downloadUrl$);
      })
    });
    console.log(this.fileControl.value);

  }

  /* private uploadImage() {
    let file = `images/${image.name}`;
    const fileRef = this.storage.ref(this.filePath);
    const task = this.storage.upload(this.filePath, image);
    task.snapshotChanges()
      .pipe(
        finalize(() => {
          fileRef.getDownloadURL().subscribe(urlImage => {
            this.downloadURL = urlImage;
            // this.savePost(post);
            //metodo que guarda el registro
          });
        })
      ).subscribe();
  } */

  alertRegistro() {
    this.toast.present("El producto fue registrado con éxito ", 2000);
  }

  llenarLaboratorios() {
    var idlab, nombrelab;
    if (this.laboratorios.length == 0) {
      this.laboratoriosBD.forEach(lab => {
        idlab = lab.payload.doc.data().id;
        nombrelab = lab.payload.doc.data().laboratorio;
        this.laboratorios.push({ value: idlab, viewValue: nombrelab });
      });
    }
  }

  llenarCategorias() {
    var idcat, nombrecat;
    if (this.categorias.length == 0) {
      this.categoriasBD.forEach(cat => {
        idcat = cat.payload.doc.data().id;
        nombrecat = cat.payload.doc.data().categoria;
        this.categorias.push({ value: idcat, viewValue: nombrecat });
      });
    }
  }

  resetCombo() {
    this.selectedCategory = '';
    this.selectedLaboratory = '';
    this.catS = '';
    this.laboS = '';
  }

  existeLab(event: MatSelectChange) {
    this.laboS = event.value;
  }
  existeCategoria(event: MatSelectChange) {
    this.catS = event.value;
  }
  // private submitForm(formData: any, formDirective: FormGroupDirective): void {
  //   formDirective.resetForm();
  //   this.registroProductoFormGroup.reset();
  // }

  // onSubmitFunction(event) {
  //   event.currentTarget.reset();
  //   this.registroProductoFormGroup.reset();
  // }
  registrarProducto() {
    console.log(this.registroProductoFormGroup);

    var nom = this.registroProductoFormGroup.get('nombre').value;
    var cant = this.registroProductoFormGroup.get('cantidad').value;
    var pre = this.registroProductoFormGroup.get('precio').value;
    var desc = this.registroProductoFormGroup.get('descripcion').value;
    var fech = this.registroProductoFormGroup.get('fechaV').value;
    var foto = this.fileControl.value
    if (nom == "" || cant == "" || pre == "" || desc == "" || fech == "" || foto == "" || nom == null || cant == null || pre == null || desc == null
      || fech == null || foto == null || this.catS.length == 0 || this.laboS.length == 0) {
      this.toast.present("Debe llenar los campos obligatorios", 1000);
    } else {

      if (this.registroProductoFormGroup.invalid || this.fileControl.errors) {  //esta mal con datos invalidos
        this.toast.present("Debe llenar los campos con Datos Validos", 1000);
      } else {

        this.firestore.collection('productos').doc(this.registroProductoFormGroup.controls.nombre.value).set({
          nombre: this.registroProductoFormGroup.controls.nombre.value,
          cantidad: this.registroProductoFormGroup.controls.cantidad.value,
          precio: this.registroProductoFormGroup.controls.precio.value,
          descripcion: this.registroProductoFormGroup.controls.descripcion.value,
          categoria: this.selectedCategory,
          fabricante: this.selectedLaboratory,
          imagen: this.registroProductoFormGroup.controls.imagen.value
        }).then(() => {
          console.log("Producto creado");

        }).catch(error => {
          console.log(error);
        })
        setTimeout(() => {
          this.myForm.resetForm();
          this.registroProductoFormGroup.reset();
        }, 0);
        this.resetCombo();
        this.alertRegistro();
        this.buildForm();
        this.router.navigate(['index']);
      }
    }
  }
}
