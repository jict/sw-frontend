import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators } from "@angular/forms";

@Injectable({
    providedIn: 'root'
  })
  export class ProductoService {
    private numberPattern: any = /^[0-9]+$/;
    constructor() { }
  
    form: FormGroup = new FormGroup({
      nombre: new FormControl('', [Validators.required, Validators.maxLength(30)]),
      cantidad: new FormControl('', [Validators.required, Validators.min(1), Validators.max(99), Validators.pattern(this.numberPattern)]),
      precio: new FormControl('', [Validators.required, Validators.min(1), Validators.maxLength(4), Validators.pattern(this.numberPattern)]),
      descripcion: new FormControl('', [Validators.required, Validators.maxLength(500)]),
      fechaVenc: new FormControl(''),
  
    });
  
    
    
 

    initializeFormGroup() {
      this.form.setValue({
          nombre: '',
          cantidad: '',
          precio:'',
          descripcion:'',
          fechaVenc:''
        
      });
    }
  }