import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(
    public toastController: ToastController
  ) { }

  async present(message: string, time: number) {
    const toast = await this.toastController.create({
      message: message,
      duration: time,
      position: 'top'
    });
    toast.present();
  }
}
