import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import * as firebase from 'firebase/app';
import 'firebase/firestore';

@Injectable({
  providedIn: 'root'
})

export class FirebaseService {

  constructor(public db: AngularFirestore) {
    
  }

  getProductos(){
    return this.db.collection('productos').snapshotChanges();
  }   
  getCategorias(){
    return this.db.collection('categorias').snapshotChanges();
  } 
  getLaboratorios(){
    return this.db.collection('laboratorios').snapshotChanges();
  }

  /*getCategoria(categoria){
    return this.db.collection('productos',ref => ref.where('categoria', '==', categoria)
      .where('categoria', '<=', categoria + '\uf8ff'))
      .snapshotChanges();
  }*/
    
}