import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { StorageService } from './storage.service';
import { ToastService } from './toast.service';
import { first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isLoggedIn = false;

  constructor(
    public afAuth: AngularFireAuth,
    public storage: StorageService,
    public toast: ToastService,
    public modalController: ModalController,
    public router: Router,
    private firestore: AngularFirestore
  ) { }

  async loginEmail(email: string, password: string) {
    try {
      const result = await this.afAuth.signInWithEmailAndPassword(email, password);
      setTimeout(() => {
        window.location.reload()
      }, 1000);
      return result;
    } catch (error) {
      console.error(error);
    }
    /* try {
      await this.afAuth.signInWithEmailAndPassword(email, password).then(res => {
        this.isLoggedIn = true;
        this.storage.store('user', JSON.stringify(res.user));
        console.log(res);
        console.log(this.afAuth.authState.toPromise().then(res => {
          console.log(res.email);

        }));
      });
    } catch (error) {
      console.log(error);
    } */
  }

  async signinEmail(email: any, password: any, tipo: any) {
    try {
      await this.afAuth.createUserWithEmailAndPassword(email, password).then(res => {
        console.log(res);
        this.toast.present("Cuenta creada exitosamente", 2000);
        this.firestore.collection('usuarios').doc(email).set({
          email: email,
          password: password,
          tipo: tipo
        }).then(() => {
          setTimeout(() => {
            this.toast.present('Datos de usuario agregados', 3000);
          }, 3000);
        }).catch(error => {
          console.error(error);
        });
      });
    } catch (error) {
      this.modalController.dismiss();
      this.toast.present(error.message, 4000);
      console.error(error);
    }
    /* await this.afAuth.createUserWithEmailAndPassword(email, password).then(res => {
      this.toast.present("Cuenta creada exitosamente", 2000);
      this.firestore.collection('usuarios').doc(email).set({
        email: email,
        password: password,
        tipo: tipo
      }).then(() => {
        setTimeout(() => {
          this.toast.present('Datos de usuario agregados', 3000);
        }, 3000);
      }).catch(error => {
        console.error(error);
      });
    }).catch(error => {
      this.modalController.dismiss();
      this.toast.present(error.message, 4000);
    }); */
  }

  getCurrentUser() {
    return this.afAuth.authState.pipe(first()).toPromise();
  }

  async logout() {
    try {
      await this.afAuth.signOut();
      this.storage.remove('user');
      setTimeout(() => {
        window.location.reload()
      }, 1000);
    } catch (error) {
      console.error(error);
    }
    this.router.navigate(['index']);
  }

  dismiss() {
    this.modalController.dismiss();
  }

}
