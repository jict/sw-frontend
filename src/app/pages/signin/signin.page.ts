import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastService } from 'src/app/services/toast.service';
import { ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AngularFirestore } from '@angular/fire/firestore';
import { StorageService } from 'src/app/services/storage.service';
import { LoginPage } from '../login/login.page';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.page.html',
  styleUrls: ['./signin.page.scss'],
})
export class SigninPage implements OnInit {

  @Input() tipo: number;
  @Input() email: string;
  @Input() pass: string;
  @Input() uid: any;

  datosAdminFormGroup: FormGroup;
  datosFarmaciaFormGroup: FormGroup;
  datosLoginFormGroup: FormGroup;

  private numberPattern: any = /^[0-9]+$/;
  private textPattern: any = /^[a-zA-Z\s]*$/;
  private numtextPattern: any = /^[a-zA-Z0-9#.\s]*$/;
  private numtextespPattern: any = /^[a-zA-Z0-9/&quot&quot\\\]\[:;|=,+*?<>]+$/;
  public horarios = [
    { id: 1, hora: '0:00' },
    { id: 2, hora: '1:00' },
    { id: 3, hora: '2:00' },
    { id: 4, hora: '3:00' },
    { id: 5, hora: '4:00' },
    { id: 6, hora: '5:00' },
    { id: 7, hora: '6:00' },
    { id: 8, hora: '7:00' },
    { id: 9, hora: '8:00' },
    { id: 10, hora: '9:00' },
    { id: 11, hora: '10:00' },
    { id: 12, hora: '11:00' },
    { id: 13, hora: '12:00' },
    { id: 14, hora: '13:00' },
    { id: 15, hora: '14:00' },
    { id: 16, hora: '15:00' },
    { id: 17, hora: '16:00' },
    { id: 18, hora: '17:00' },
    { id: 19, hora: '18:00' },
    { id: 20, hora: '19:00' },
    { id: 21, hora: '20:00' },
    { id: 22, hora: '21:00' },
    { id: 23, hora: '22:00' },
    { id: 24, hora: '23:00' },
  ];

  constructor(
    private modalController: ModalController,
    private formBuilder: FormBuilder,
    public toast: ToastService,
    public router: Router,
    private firestore: AngularFirestore,
    public storage: StorageService
  ) {
    this.buildForm();
  }

  ngOnInit() {
    console.log(this.tipo);
    this.datosLoginFormGroup.controls.email.setValue(this.email);
    this.datosLoginFormGroup.controls.contrasena.setValue(this.pass);
    this.uid = this.storage.get('uid');
  }

  private buildForm() {
    this.datosAdminFormGroup = this.formBuilder.group({
      nombre: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30), Validators.pattern(this.textPattern)]],
      apellido: ['', [Validators.required, Validators.minLength(2), Validators.maxLength(30), Validators.pattern(this.textPattern)]],
      ci: ['', [Validators.required, Validators.minLength(6), Validators.maxLength(8), Validators.pattern(this.numberPattern)]],
      prof: ['', [Validators.required, Validators.maxLength(30), Validators.pattern(this.textPattern)]],
      cel: ['', [Validators.required, Validators.min(60000000), Validators.max(79999999), Validators.pattern(this.numberPattern)]]
    });
    this.datosFarmaciaFormGroup = this.formBuilder.group({
      name: ['', [Validators.required, Validators.maxLength(20), Validators.pattern(this.textPattern)]],
      phone: ['', [Validators.required, Validators.min(4000000), Validators.max(4999999), Validators.pattern(this.numberPattern)]],
      numnit: ['', [Validators.required, Validators.max(99999999999999999999), Validators.pattern(this.numberPattern)]],
      horaInicio: ['', [Validators.required]],
      horaFin: ['', [Validators.required]],
      dir: ['', [Validators.required, Validators.maxLength(30), Validators.pattern(this.numtextPattern)]]

    });
    this.datosLoginFormGroup = this.formBuilder.group({
      email: [{ value: '', disabled: true }, [Validators.required, Validators.email]],
      contrasena: [{ value: '', disabled: true }, [Validators.required, Validators.minLength(8), Validators.maxLength(15), Validators.pattern(this.numtextespPattern)]],
      retype_contrasena: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(15), Validators.pattern(this.numtextespPattern)]]
    });

    if (!this.tipo) {
      this.datosAdminFormGroup.controls.prof.setValue('no proveido');
    }
    this.horarioAux();
  }

  horarioAux() {
    const horarioAux = this.horarios.slice(this.datosFarmaciaFormGroup.get('horaInicio').value, this.horarios.length);
    //console.log("HORARIO AUXILIAR ");
    horarioAux.forEach(element => {
      //console.log(element.hora);
    });
    return horarioAux;
  }
  getErrorMessagePass() {
    var p1 = this.datosLoginFormGroup.get('contrasena').value;
    var p2 = this.datosLoginFormGroup.get('retype_contrasena').value;

    if (p1 != p2 || p1.length != p2.length) {
      return "Las contraseñas deben de coincidir";
    } else {
      return "Este es un campo obligatorio"
    }

  }

  verifyPass() {
    var p1 = this.datosLoginFormGroup.get('contrasena').value;
    var p2 = this.datosLoginFormGroup.get('retype_contrasena').value;
    var espacios = false;
    var cont = 0;

    while (!espacios && (cont < p1.length)) {
      if (p1.charAt(cont) == " ")
        espacios = true;
      cont++;
    }

    if (espacios) {
      this.toast.present("La contraseña no puede contener espacios en blanco", 3000);
      return false;
    }
    if (p1 != p2) {
      this.toast.present("Los passwords deben de coincidir", 3000);
      return false;
    } else {
      this.recordUsuario();
    }
  }

  recordUsuario() {
    if (this.tipo) { //CASE FARMACIA
      this.firestore.collection('usuarios').doc(this.email).update({
        nombre: this.datosAdminFormGroup.controls.nombre.value,
        apellido: this.datosAdminFormGroup.controls.apellido.value,
        ci: this.datosAdminFormGroup.controls.ci.value,
        prof: this.datosAdminFormGroup.controls.prof.value,
        cel: this.datosAdminFormGroup.controls.cel.value,
        name: this.datosFarmaciaFormGroup.controls.name.value,
        phone: this.datosFarmaciaFormGroup.controls.phone.value,
        numnit: this.datosFarmaciaFormGroup.controls.numnit.value,
        horaInicio: this.datosFarmaciaFormGroup.controls.horaInicio.value,
        horaFin: this.datosFarmaciaFormGroup.controls.horaFin.value,
        dir: this.datosFarmaciaFormGroup.controls.dir.value
      }).then(() => {
        this.toast.present("Datos de farmacia actualizados", 3000);
        this.dismiss();
        this.modalasd();
      }).catch(error => {
        console.log(error);
      });
    } else { //CASE CLIENTE
      this.firestore.collection('usuarios').doc(this.email).update({
        nombre: this.datosAdminFormGroup.controls.nombre.value,
        apellido: this.datosAdminFormGroup.controls.apellido.value,
        ci: this.datosAdminFormGroup.controls.ci.value,
        cel: this.datosAdminFormGroup.controls.cel.value
      }).then(() => {
        this.toast.present("Datos de cliente actualizados", 3000);
        this.dismiss();
        this.modalasd();
      }).catch(error => {
        console.log(error);
      });
    }
  }

  async modalasd() {
    const modal = await this.modalController.create({
      component: LoginPage,
      componentProps: {
        'email': this.email
      }
    });
    return await modal.present();
  }

  dismiss() {
    this.modalController.dismiss();
  }
}
