import { Component, Input, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  @Input() email: string;

  public logInFormGroup: FormGroup;

  private numtextespPattern: any = /^[a-zA-Z0-9/&quot&quot\\\]\[:;|=,+*?<>]+$/;

  constructor(
    private modalController: ModalController,
    public formBuilder: FormBuilder,
    public authService: AuthService,
    public router: Router,
    public afStore: AngularFirestore
  ) {
    this.buildForm();

  }

  ngOnInit() {
    if (this.email) {
      this.logInFormGroup.controls.email.setValue(this.email);
    }
  }

  buildForm() {
    this.logInFormGroup = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      pass: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(15), Validators.pattern(this.numtextespPattern)]]
    });
  }

  loginEmail() {
    this.authService.loginEmail(this.logInFormGroup.controls.email.value, this.logInFormGroup.controls.pass.value);
    this.dismiss();
  }

  dismiss() {
    this.modalController.dismiss();
  }
}
