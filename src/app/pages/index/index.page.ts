import { MatDialog, MatDialogModule } from '@angular/material/dialog';
import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from 'src/app/services/auth.service';
import { StorageService } from 'src/app/services/storage.service';


@Component({
  selector: 'app-index',
  templateUrl: './index.page.html',
  styleUrls: ['./index.page.scss'],
})
export class IndexPage implements OnInit {

  public email: any;
  public isLoggedIn: any;

  constructor(
    public afStore: AngularFirestore,
    public storage: StorageService,
    public afAuth: AuthService
  ) {
  }

  ngOnInit() {
    this.storage.get('user').then(res => {
      const user = JSON.parse(res)
      console.log(user.email)
    });
    // this.isLoggedIn = this.afAuth.getAuthState();
    console.log(this.isLoggedIn);

  }
}
