import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { SelectModePage } from '../select-mode/select-mode.page';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage implements OnInit {

  constructor(
    private modalController: ModalController
  ) { }

  ngOnInit() {
  }

  dismiss(tipo: number) {
    console.log(tipo);
    if (tipo < 2) {
      this.registro(tipo);
    }
    this.modalController.dismiss();
  }

  async registro(tipo: number) {
    const modal = await this.modalController.create({
      component: SelectModePage,
      componentProps: { 'tipo': tipo }
    });
    return await modal.present();
  }
}
