import { Input } from '@angular/core';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ModalController } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { SigninPage } from '../signin/signin.page';

@Component({
  selector: 'app-select-mode',
  templateUrl: './select-mode.page.html',
  styleUrls: ['./select-mode.page.scss'],
})
export class SelectModePage implements OnInit {

  @Input() tipo: number;
  public signInFormGroup: FormGroup;
  public uid: any;

  private numtextespPattern: any = /^[a-zA-Z0-9/&quot&quot\\\]\[:;|=,+*?<>]+$/;

  constructor(
    public modalController: ModalController,
    public formBuilder: FormBuilder,
    public authService: AuthService
  ) {
    this.buildForm();
  }

  ngOnInit() {
    console.log("Tipo de usuario: 1->Farmacia : 0->Cliente", this.tipo);
  }

  buildForm() {
    this.signInFormGroup = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      pass: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(15), Validators.pattern(this.numtextespPattern)]]
    });
  }

  signinEmail() {
    this.authService.signinEmail(this.signInFormGroup.controls.email.value, this.signInFormGroup.controls.pass.value, this.tipo).then(res => {
      console.log(res);

      this.dismiss();
      this.signInEmailModal(this.signInFormGroup.controls.email.value, this.signInFormGroup.controls.pass.value, res);
    }).catch(error => {
      console.error(error);

    });
  }

  async signInEmailModal(email: string, pass: string, uid: any) {
    const modal = await this.modalController.create({
      component: SigninPage,
      componentProps: {
        'tipo': this.tipo,
        'email': email,
        'pass': pass,
        'uid': uid
      }
    });
    return await modal.present();
  }

  dismiss() {
    this.modalController.dismiss();
  }
}
