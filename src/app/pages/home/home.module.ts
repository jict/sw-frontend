import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';
import { HomePage } from './home.page';

import { ComponentsModule } from '../../components/components.module';

import { HomePageRoutingModule } from './home-routing.module';

import { ElementsMenuModule } from '../../elementsMenu/elementsMenu.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomePageRoutingModule,
    ComponentsModule,
    ElementsMenuModule
  ],
  declarations: [HomePage]
})
export class HomePageModule { }
