// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDqI7kLexl35dl5oCf5ujVV68o_hlxVq4k",
    authDomain: "software-aad77.firebaseapp.com",
    databaseURL: "https://software-aad77.firebaseio.com",
    projectId: "software-aad77",
    storageBucket: "software-aad77.appspot.com",
    messagingSenderId: "402805685022",
    appId: "1:402805685022:web:123e4e30b39e69b9a8670f"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
